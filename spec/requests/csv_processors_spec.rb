require 'rails_helper'

RSpec.describe 'CSV Processor API' do
  let(:headers) { valid_headers }
  let(:csv_file) { File.new(Rails.root + 'sample.csv') }
  let(:malformed_csv) { File.new(Rails.root + 'malformed_sample.csv') }


  describe 'POST /csv_processors' do

    context 'when request is valid' do
      before { post '/csv_processors',params: {csv: fixture_file_upload(csv_file, 'text/csv')}, headers: headers }

      it 'parse csv and count users first name' do
        expect(json["Umair"]).to eq(3)
      end

      it 'returns status code 201' do
        expect(response).to have_http_status(200)
      end

    end

    context 'when csv file is not valid'
    before { post '/csv_processors',params: {csv: fixture_file_upload(malformed_csv, 'text/csv')}, headers: headers }
    it 'should return malformed csv error' do
      expect(response).to have_http_status(422)
    end


  end


end
