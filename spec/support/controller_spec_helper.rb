module ControllerSpecHelper

  # return valid headers
  def valid_headers
    {
      "Authorization" => "test",
      "Content-Type" => "application/json"
    }
  end

  # return invalid headers
  def invalid_headers
    {
      "Authorization" => nil,
      "Content-Type" => "application/json"
    }
  end
end
