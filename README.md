# README

This README would normally document whatever steps are necessary to get the
application up and running.

# Installation:

## Backend API

* ruby 2.4.0

* Rails 5.1.1

* git clone git@bitbucket.org:umairejaz/csv-api.git

* cd csv-api

* bundle install

* bundle exec rspec

* rails server -p 3000

## Frontend Application

* ruby 2.4.0

* Rails 5.1.1

* git clone git@bitbucket.org:umairejaz/csv-parser.git

* cd csv-parser

* bundle install

* rails server -p 3001


