require 'csv'
module V1
  class CsvProcessorsController < ApplicationController

    # TODO implement token base authorization on API

    #POST /csv_processors
    # @param File csv
    def create

      extracted_data   = CSV.table(params[:csv].path)
      transformed_data = extracted_data.map { |row| row.to_hash }

      freq = Hash.new(0)
      transformed_data.each { |element| freq[element[:first_name]] += 1 }
      json_response(freq.map{ |key, value| [key,value] }.to_h)

    end

  end
end

